<?php
	require_once('includes/mpdf/mpdf.php');
	include "includes/connect.php";
	//Order ID is obtained from get request
	$orderID = $_GET['orderID'];
	
	//Get the shopper ID, address id from a query
	$stmt = $db -> prepare('Select *
							FROM `Order`, OrderStatus, `Status`, Shaddr, (
								SELECT Max(OrderStatus.OrderStatus_id) AS Status_id, OrderStatus.OrderStatus_Order_id AS Order_id
								FROM OrderStatus
								GROUP BY Order_id) i
							WHERE `Order`.Order_id = i.Order_id
							AND OrderStatus.OrderStatus_id = i.Status_id
							AND OrderStatus.OrderStatus_Status_id = `Status`.Status_id
							AND Order.Order_Shaddr = Shaddr.shaddr_id
							AND Order.Order_id = ?;');
	$stmt -> bindParam(1, $orderID);
	$html = "";
	
	if($stmt->execute())
	{
		if($order = $stmt->fetch())
		{
			$html .= '<html><head><title>Print Label</title><style> p {font-size:22px; } </style></head>
						<body ><table border=1 width=100%><tr><td>
							<table id="header" width=100%>			
								<tr><td><p>'.$order["sh_title"].' '.$order["sh_firstname"].' '.$order["sh_familyname"].'</p></td></tr>
								<tr><td><p>'.$order["sh_street1"].'</p></td></tr>
								<tr><td><p>'.$order["sh_street2"].'</p></td></tr>
								<tr><td><p>'.$order["sh_city"].'</p></td></tr>
								<tr><td><p>'.$order["sh_state"].' '.$order["sh_postcode"].' '.$order["sh_country"].'</p></td></tr>
							</table></td></tr></table>';
		}
	}
	$html .= "</body></html>";
	$mpdf = new mPDF('utf-8', array(130,82));
	$mpdf->SetJS('this.print();');
	$mpdf->WriteHTML($html);

	$mpdf->Output();

	exit;

?>