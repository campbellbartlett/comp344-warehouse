<?php
//connect.php - database independent connection using PHP::PDO
// file currently supports SQLite and MYSQL, but can easily be edited for other database types


//change this variable to change database type
$dbloc = "mysql"; // Use "mysql", "sqlite" or add others as appropriate

if ($dbloc == "sqlite") {
	$dbhost = "database.db"; //only the path to the .db SQLite file is required
	
}
else if ($dbloc == "mysql") { //All variables are required to connect


	$dbhost = 'comp344-warehouse.com'; //MYSQL hostname
	$dbusername = 'comp344w_user'; //Database username
	$dbuserpassword = '123SQL'; //Database password
	$default_dbname = 'comp344w_shop'; //Database instance name
	
}
else {
	//error or you can add other DB vendors here
}

// Use PDO to connect to the database; return the PDO object
function db_connect() {
    global $dbloc, $dbhost, $dbusername, $dbuserpassword, $default_dbname;
    // Set a default exception handler
    set_exception_handler("store_exception_handler");
    
    
    if ($dbloc == "sqlite") { // SQLite Connection
	    $db = new PDO("sqlite:".$dbhost);
    }
    else if ($dbloc == "mysql") { //MYSQL Connection
    	$db = new PDO("mysql:host=$dbhost;dbname=$default_dbname;charset=utf8", $dbusername, $dbuserpassword);
    }
    
    $db->setAttribute(PDO::ATTR_PERSISTENT, true);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    
	return $db;
}

function store_exception_handler(RuntimeException $ex) {
	$debug = true;		// If true, report to screen; otherwise silently log and die.
	if(get_class($ex) == "PDOException") {
		if ($debug == true)
			echo "PDO Exception in file " . basename($ex->getFile()) . ", line " . $ex->getLine() . ":<br/>Code " . $ex->getCode() . " - " . $ex->getMessage();
		 else 
			error_log("PDO Exception in file " . basename($ex->getFile()) . ", line " . $ex->getLine() . ": Code " . $ex->getCode() . " - " . $ex->getMessage());
	}
	else {
		error_log("Unhandled Exception in file " . basename($ex->getFile()) . ", line " . $ex->getLine() . ": Code " . $ex->getCode() . " - " . $ex->getMessage());
	// Any other unhandled exceptions will wind up at the store home page, for safety
	header("Location: index.php");
	}
}

//create a database object to be used by all pages
$db = db_connect();
if($dbloc=='mysql')
{
	$db ->exec('use comp344w_shop');
}
?>