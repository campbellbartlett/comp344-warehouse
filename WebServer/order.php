<?php

	/* 
	
	Order.php 
	
	Created by: 	Campbell Bartlett
					Miassar Gebara
					David Hajje
					Nicholas Jenkins
					Brenda Siddharta
	
	Comp344 - Group Assignment - 2016

	*/

	/*
		Structure of this file:

		- Read query string from URL
		- Read POST data and perform any queries based on that data 
		- Retrieve order information for the orderID which was from query string
			- Display the order information (including warehouse_comment)
			- Query the database to get a list of items in the order
			- Query the database to check if each item has any attributes
			- Display each item and its attributes (if applicable)
			- Display buttons for printing pick slip, printing label and marking the order
		 	  as shipped or picked
			- If the order is already marked as shipped or picked then disable the respective button
			- If the order is changed to shipped then email the shopper the tell them their order is being shipped
		- If the orderID was not found in the database then display an error message for the user	

	*/

	

	// Connect.php contains the database connection
	include "includes/connect.php";

	//Order ID is obtained from get request
	$orderID = $_GET['orderID'];
	if (!empty($_POST))
	{
		/* 
		 If there is data in $_POST then either:
		 1. The warehouse comment has been changed and save comment has been clicked
		 2. The order status is changing from 'Order Paid' to 'Order Picked'
		 3. The order status is changing from 'Order Picked' to 'Order Shipped'
		 Each of these three options require a query to be run to UPDATE or INSERT
		 data in the database

		 The statusIDs for shipped and picked.
		 These are taken from the database and are hard coded to recude the number 
		 of queries that need to be run.
		*/

		$SHIPPED_STATUS = 4;
		$PICKED_STATUS = 3;

		// If there is a warehouse_comment in $_POST then update the comment in the database
		if (!empty($_POST['warehouse_comment']))
		{
			// Get the warehouse comment from the post values.
			$dirtyWarehouseComment = $_POST['warehouse_comment'];
			
			//sanitise the database input
			$warehouseComment = filter_var($dirtyWarehouseComment, FILTER_SANITIZE_STRING);
			
			// Query to save changes to the warehouse comment
			$commentStmt = $db -> prepare('UPDATE `Order`
											SET Order_NoteForWarehouseStaff = ?
											WHERE Order_id = ?');
			// Bind the parameterts
			$commentStmt -> bindParam(1, $warehouseComment);
			$commentStmt -> bindParam(2, $orderID);
		
			// Run the query
			if ($commentStmt->execute())
			{
				// Confirm with the user that the comment has been updated
				echo "<script>alert('Warehouse comment updated')</script>";
			}
		}
		// If there is no warehouse_comment but there is a value for is_picked then update the status of 
		// the order to 'Order Picked'
		elseif (!empty($_POST['is_picked']))
		{
			// Confirm that the is_picked value is the value that is expected
			if ($_POST['is_picked'] == "set")
			{
				// Set the query
				$updateStmt = $db -> prepare('INSERT INTO OrderStatus (OrderStatus_Order_id, OrderStatus_Status_id, OrderStatus_DateTime)
											  VALUES (?, ?, now())');
				// Bind query values
				$updateStmt -> bindParam(1, $orderID);
				$updateStmt -> bindParam(2, $PICKED_STATUS);

				// Execute query
				if ($updateStmt -> execute())
				{
					// Confirm to the user that the status has been updated.
					echo "<script>alert('Order status updated')</script>";
				}
			}
		} 
		// If there is no warehouse_comment but there is a value for is_shipped then update the status of 
		// the order to 'Order Picked'
		elseif (!empty($_POST['is_shipped']))
		{
			// Confirm that the is_set value is the value that is expexted.
			if ($_POST['is_shipped'] == "set")
			{
				// Set the query
				$updateStmt = $db -> prepare('INSERT INTO OrderStatus (OrderStatus_Order_id, OrderStatus_Status_id, OrderStatus_DateTime)
											  VALUES (?, ?, now())');
				// Bind query values
				$updateStmt -> bindParam(1, $orderID);
				$updateStmt -> bindParam(2, $SHIPPED_STATUS);

				// Execute query
				if ($updateStmt -> execute())
				{
					// Confirm to the user that the status has been updated.
					echo "<script>alert('Order status updated')</script>";
					
					// When an order status is changed to shipped, an email is sent to the
					// shopper to confirm that their order has been shipped.

					// Query to get the shoppers email address
					$emailStmt = $db -> prepare("SELECT * FROM Shopper, `Order`, Shaddr
												WHERE Shopper.Shopper_id = Order_id
												AND Shaddr.shopper_id = Shopper.shopper_id
												AND Order_id = ?");
					// Bind the values on the query
					$emailStmt -> bindParam(1, $orderID);

					// Execute the query
					if ($emailStmt -> execute())
					{
						// If the query executes correctly then email the shopper
						if($row= $emailStmt->fetch())
						{
							// Email.php contains all the values for our email service.
							require ('includes/email.php');

							$mail = new PHPMailer; 				//create new mail object
							$mail->isSMTP();                  	// Set mailer to use SMTP
							$mail->Host = $SMTPHostName;		// Retrieve the hostname from email.php
							$mail->SMTPAuth = $SMTPAuth;    	// Retrieve the SMTP Auth type from email.php                           
							$mail->Username = $SMTPUsername;	// Retrieve the username from email.php              
							$mail->Password = $SMTPPassword;    // Retrieve the password from email.php                     
							$mail->SMTPSecure = $SMTPSecurity;  // Retrieve the type of connection from email.php                
							$mail->Port = $SMTPPort;            // Rerieve the SMTP port form email.php                        							$mail->From = $SMTPFromAddress;
							$mail->FromName = $SMTPFromDisplay; // Set the from name for the email.

							// Create a recepient using the shoppers first and last names
							$recepient = $row['sh_firstname']." ".$row['sh_familyname'];  

							$mail->addAddress($row['sh_email'], $recepient);     		 		// Add a recipient
							$mail->isHTML(true);                                  		 		// Set email format to HTML
							$mail->Subject = "COMP344 Store: Your order has been shipped!!";	// Set the subject for the email.
							
							// Set the email body using inline css for formatting.
							$mail->Body = "<html>
											<body>
												<p style=\"color:#777777; font-family: 'Raleway', sans-serif; font-size: 18px; display:block;text-align: center;\">Dear ".$row['sh_title']." ".$row['sh_firstname']." ".$row['sh_familyname']."</p><br />
												<br />
												<p style=\"color:#777777; font-family: 'Raleway', sans-serif; font-size: 18px; display:block;text-align: center;\">This is a message to inform you that your order #".$orderID." has been shipped.</p><br />
												<br />
												<br />
												<p style=\"color:#777777; font-family: 'Raleway', sans-serif; font-size: 18px; display:block;text-align: center;\">Regards,<br />
												The team at Comp344 Store</p>
											</body>

										</html>";
							// Try to send the email
							if(!$mail->send()) {
								// If the email cannot send then output to the user that the email failed.
								print 'Mail could not be sent';
							} 
							else 
							{
								// Mail has been sent succesfully.
								// No need to do anything.
							}
						}
					}
					
					
				}
			}
		}
		else 
		{
			// POST request with no valid post data. ~ Do nothing.
		}
	}

	
	
	//Get the shopper ID, address id from a query
	$stmt = $db -> prepare('Select *
							FROM `Order`, OrderStatus, `Status`, Shaddr, (
								SELECT Max(OrderStatus.OrderStatus_id) AS Status_id, OrderStatus.OrderStatus_Order_id AS Order_id
								FROM OrderStatus
								GROUP BY Order_id) i
							WHERE `Order`.Order_id = i.Order_id
							AND OrderStatus.OrderStatus_id = i.Status_id
							AND OrderStatus.OrderStatus_Status_id = `Status`.Status_id
							AND Order.Order_Shaddr = Shaddr.shaddr_id
							AND Order.Order_id = ?;');
	// Bind the parameters on the query
	$stmt -> bindParam(1, $orderID);
	// Execute the query
	if($stmt->execute())
	{
		// If there is a result then display the order
		if($order = $stmt->fetch())
		{
		
			// Using results from the query, construct a dynamic HTML page using echo
			// that shows all the order details
			echo "<html>
				<head>
					<title>Viewing order #".$orderID."</title>
					<link href=\"css/order.css\" rel=\"stylesheet\" type=\"text/css\" />
				</head>
				<body>
					<a href=\"index.php\"> <img id='logo' src='images/logo.png' /> </a>
					
					<heading class=\"heading\" > <strong> View Order </strong> </heading>
					<hr>
					<h1> Order #".$orderID."</h1>
					<table class=\"table1\" >
						<tr>
							<th>Shipping Address</th><th colspan='2'>Order Details</th>
						</tr>
						<tr>
							<td>
								<p>".$order['sh_title']." ".$order['sh_firstname']." ".$order['sh_familyname']."</p>
								<p>".$order['sh_street1']."<br />".$order['sh_street2']."<br />".$order['sh_city']."<br />".$order['sh_state']." ".$order['sh_postcode']."<br />".$order['country']."</p>
							</td>
							<td>
								<p><strong>Source</strong><br />
								COMP344 Store</p>
								<p><strong>Order Date</strong><br />".$order['Order_TimeStamp']."</p>
								</td>
								<td>
								<p><strong>Order Status</strong>
								<br />".$order['Status_Name']."</p>
								<form id='comment-form' method ='post' action='order.php?orderID=".$orderID."'>
									<p><strong>Warehouse Comment</strong>
									<br /><textarea name='warehouse_comment' rows='4' cols='35'>".$order['Order_NoteForWarehouseStaff']."</textarea></p>
									<button type=\"button\" class=\"button\" style=\"margin-left:0px;\padding:10px;\margin-top:0px;\" onclick=\"document.getElementById('comment-form').submit();\";\">Save comment</button>

								</form>

							</td>
							
						</tr>

					</table>
					<br>
					<h2>Products Ordered</h2>
					<table class=\"table2\">
						<tr>
							<th>Product ID</th><th>Item</th><th>Ordered</th><th>Attributes</th>
						</tr>";

						// Get all the items from the order. The query will ony retrieve items within the order
						// without worrying about attributes.
						$stmt = $db -> prepare('SELECT * FROM OrderProduct, Product
												WHERE OP_Order_id = ?
												AND OrderProduct.OP_prod_id = Product.prod_id;');
						
						// Bind the attributes on the query
						$stmt -> bindParam(1, $orderID);
						// Execute the query
						if($stmt->execute())
						{
							if($items = $stmt->fetchAll())
							{
								// If the query returns any results then
								// check if each item has any attributes
								foreach($items as $item)
								{
									// Query that displays the attributes of each item for one order
									$stmt = $db -> prepare('SELECT  OrderProduct.OP_Order_id as OrderId, OrderProduct.OP_prod_id as ProductId, Product.prod_name as ProductName, Attribute.name as Attribute, AttributeValue.AttrVal_Value as AttributeValue, OrderProduct.OP_prod_id as Quantity
															FROM    OrderProduct, Attribute, AttributeValue, OrderProductAttributeValues, Product, `Order`
															WHERE   OrderProduct.OP_Order_id = OrderProductAttributeValues.OPAttr_op_id
															AND     OrderProductAttributeValues.OPAttr_Attr_id = Attribute.id
															AND     OrderProductAttributeValues.OpAttr_AttrVal_id = AttributeValue.AttrVal_id
															AND     OrderProduct.OP_prod_id = Attribute.Product_prod_id
															AND     Product.prod_id = OrderProduct.OP_prod_id
															AND		OrderProduct.OP_Order_id = ?
															AND 	OrderProduct.OP_prod_id = ?
															GROUP   BY AttributeValue.AttrVal_id');
									// Bind the attributes on the query
									$stmt -> bindParam(1, $orderID);
									$stmt -> bindParam(2, $item['OP_prod_id']);
									// Execute the query
									if($stmt->execute())
									{
										// if there are results then print out each attribute has a new item
										if($currentItem = $stmt->fetchAll())
										{
											foreach($currentItem as $attribute)
											{
												echo "<tr>
														<td><p>".$attribute['ProductId']."</p></td>
														<td><p>".$attribute['ProductName']."</p></td>
														<td><p>".$attribute['Quantity']."</p></td>
														<td><p>".$attribute['Attribute'].": ".$attribute['AttributeValue']."</p></td>
													</tr>";
												
												
											}
										}
										//otherwise just print it without any attributes
										else
										{
											echo "<tr>
													<td><p>".$item['OP_prod_id']."</p></td>
													<td><p>".$item['prod_name']."</p></td>
													<td><p>".$item['OP_qty']."</p></td>
													<td><p></p></td>
												</tr>";
										}
									}
								}
							}
						}
					echo "</table>";

					// Print buttons
					echo "<button type=\"button\" id=\"packingSlip\" class=\"button\" onclick=\"document.location.href='printOrder.php?orderID=".$orderID."'\">Print packing slip</button>";
					echo "<button type=\"button\" id=\"printLabel\" class=\"button\" onclick=\"document.location.href='printLabel.php?orderID=".$orderID."'\">Print label</button>";

					// Order Picked Button

					// If the orderStatus is 'Order Picked' then disable the 'Mark as picked' button
					// and change the button ID to #pickedDisabled
					$pickedDisabled = "";
					$pickedButtonID = "";
					if ($order['Status_Name'] == "Order Picked" || $order['Status_Name'] == "Order Shipped")
					{
						$pickedDisabled = "disabled";
						$pickedButtonID = "Disabled";
					}
						
					echo "<form id='picked-form' method ='post' action='order.php?orderID=".$orderID."'> ";
						echo "<button type=\"submit\" id=\"picked" . $pickedButtonID . "\"" . $pickedDisabled .">Mark as picked</button>";
						// A hidden field is set to 'set' when the button is clicked
						echo "<input type='hidden' value='set' name='is_picked'/>";
					echo "</form>";

					// Mark as shipped button embedded in a form

					// If the orderStatus is 'Order Shipped' then disable the 'Mark as shipped' button
					// and change the button ID to #shippedDisabled
					$shippedDisabled = "";
					$shippedButtonID = "";
					if ($order['Status_Name'] == "Order Shipped")
					{
						$shippedDisabled = "disabled";
						$shippedButtonID = "Disabled";
					}
					echo "<form id='shipped-form' method ='post' action='order.php?orderID=".$orderID."'> ";
						echo "<button type=\"submit\" id=\"shipped" . $shippedButtonID . "\"" . $shippedDisabled .">Mark as shipped</button>";
						// A hidden field is set to 'set' when the button is clicked
						echo "<input type='hidden' value='set' name='is_shipped'/>";
					echo "</form>";
				echo "</body></html>";
				
		}
		else
		{
			// If there are no results to the query that retrieves the order from the database
			// then no order with the ID from the GET query exists.
			echo "<html>
					<head><title>Viewing order #".$orderID."</title></head>
					<h1>Sorry, order id #".$orderID." does not exist.</h1>
				</html>";
		}
	}

?>