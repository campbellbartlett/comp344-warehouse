 
<html>
	<head>
		<title>COMP344 Warehouse</title>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="COMP344" content="">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/indexcustom.css" rel="stylesheet">
		<link href="css/order.css" rel="stylesheet">
		<script src="css/jquery/jquery-1.10.2.js"></script>
		<script src="css/jquery/jquery-ui.js"></script>
		
		<script>
			$(document).ready(function() { 

			$("#searchbutton").click(function() {	
										$(".search").slideToggle();
			 
										
									});
								});
			
		</script>
		
		
	</head>
	<body>
		<div class="container">
			<h1> COMP344 WAREHOUSE </H1>
			<hr>
		</div>
		<br>
		<br>
		<div class="container">
		
			<div class='buttonpanel'>
				<a href="index.php?DataEntry=ALL" class="orderbuttonpanelAll" role ="button">   All Orders</a>
				<a href="index.php?DataEntry=PENDING" class="orderbuttonpanelPen" role ="button">   Pending Orders</a>
				<a href="index.php?DataEntry=PICKED" class="orderbuttonpanelPick" role ="button">  Picked Orders</a>
				<a href="index.php?DataEntry=SHIPPED" class="orderbuttonpanelShip" role ="button">  Shipped Orders</a>
				<a  class="orderbuttonpanel" id="searchbutton">  Search</a>
			</div>
			<div class="content">
				<hr>
				<div class='search'>
					<br>
					
					 <div id="searchform1">
					 <br>
						 <form method='get' >
							  <h3>Search by Order ID</h3><br>
							  <input type="number" name="order_id">
							  <input type="submit" action='' value="Search" >
					</form></div> 
					<div id="searchform2">
					<br>
							<form method='get'>
							  <h3>Search by Shopper ID</h3><br>
							  <input type="number" name="shopper_id" >
							  <input type="submit" action='' value="Search" >
					</form></div>
					<div id="searchform3">
					<br>
							<form method='get'>
							  <h3>Search by Product ID</h3><br>
							  <input type="number" name="product_id" >
							  <input type="submit" action='' value="Search" >
					</form></div>
					<div id="searchform4">
							<form method='get'>
							  <h3>Start Date
							  <input type="date" name="start_date" id="datepicker">
							  AND /OR End Date
							  <input type="date" name="end_date" id="datepicker1"> </h3>
							  
							  <input type="submit" action='' value="Search" id=searchid >
							 
					</form></div>

										 <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
										 <hr>
											 
					</div>
					</div>
				</div>
				
			
				<?php
				
				
			
				include "includes/connect.php";
								if(isset($_GET['order_id']))
								{ print "<h1><center>Order ID Search</center></h1>";
									 
										$order_id = $_GET["order_id"];
											$stmt = $db ->prepare( "Select *
														FROM `Order`, OrderStatus, `Status`, (
															SELECT Max(OrderStatus.OrderStatus_id) AS Status_id, OrderStatus.OrderStatus_Order_id AS Order_id
															FROM OrderStatus
															GROUP BY Order_id) i
														WHERE `Order`.Order_id = i.Order_id
														
														AND OrderStatus.OrderStatus_id = i.Status_id
														AND OrderStatus.OrderStatus_Status_id = `Status`.Status_id
														AND `Order`.Order_id = ?");
											$stmt -> bindParam(1, $order_id);
														
								}else if(isset($_GET['shopper_id']))
								{ print "<h1><center>Shopper ID Search</center></h1>";
									 
										$shopper_id = $_GET["shopper_id"];
											$stmt = $db ->prepare( "Select *
														FROM `Order`, OrderStatus, `Status`, (
															SELECT Max(OrderStatus.OrderStatus_id) AS Status_id, OrderStatus.OrderStatus_Order_id AS Order_id
															FROM OrderStatus
															GROUP BY Order_id) i
														WHERE `Order`.Order_id = i.Order_id
														
														AND OrderStatus.OrderStatus_id = i.Status_id
														AND OrderStatus.OrderStatus_Status_id = `Status`.Status_id
														AND `Order`.Order_Shopper = ?");
											$stmt -> bindParam(1, $shopper_id);
								}else if(isset($_GET['product_id']))
								{ print "<h1><center>Product ID Search</center></h1>";
									 
										$product_id = $_GET["product_id"];
											$stmt = $db ->prepare( "Select *
														FROM `Order`, OrderStatus, `Status`, OrderProduct, (
															SELECT Max(OrderStatus.OrderStatus_id) AS Status_id, OrderStatus.OrderStatus_Order_id AS Order_id
															FROM OrderStatus
															GROUP BY Order_id) i
														WHERE `Order`.Order_id = i.Order_id
														AND `Order`.Order_id = `OrderProduct`.OP_Order_id
														AND OrderStatus.OrderStatus_id = i.Status_id
														AND OrderStatus.OrderStatus_Status_id = `Status`.Status_id
														AND `OrderProduct`.OP_prod_id = ?");

											$stmt -> bindParam(1,$product_id);
								}else if(isset($_GET['start_date']))
								{ print "<h1><center>Search by Date range</center></h1>";
									 
									$start_date = strtotime($_GET['start_date']);
									$start_date = date("Y/m/d", $start_date);
									
									if(empty($_GET['end_date']))
									{
										$end_date = date('Y/m/d', time());
									}
									else
									{
										$end_date = strtotime($_GET['end_date']);
										$end_date = date("Y/m/d", $end_date);
									}
									
										
										$stmt = $db ->prepare( "Select *
														FROM `Order`, OrderStatus, `Status`, (
															SELECT Max(OrderStatus.OrderStatus_id) AS Status_id, OrderStatus.OrderStatus_Order_id AS Order_id
															FROM OrderStatus
															GROUP BY Order_id) i
														WHERE `Order`.Order_id = i.Order_id
														
														AND OrderStatus.OrderStatus_id = i.Status_id
														AND OrderStatus.OrderStatus_Status_id = `Status`.Status_id
														AND `Order`.Order_TimeStamp >= ?
														AND `Order`.Order_TimeStamp <= ?");	
														
										$stmt -> bindParam(1, $start_date);
										$stmt -> bindParam(2, $end_date);
								} 
								else						
									if(isset($_GET['DataEntry']))
									{
										if($_GET['DataEntry'] == "ALL"){ //show all orders
											print "<h1><center>All Orders</center></h1>";
											$stmt = $db ->prepare( 'Select *
														FROM `Order`, OrderStatus, `Status`, (
															SELECT Max(OrderStatus.OrderStatus_id) AS Status_id, OrderStatus.OrderStatus_Order_id AS Order_id
															FROM OrderStatus
															GROUP BY Order_id) i
														WHERE `Order`.Order_id = i.Order_id
														AND OrderStatus.OrderStatus_id = i.Status_id
														AND OrderStatus.OrderStatus_Status_id = `Status`.Status_id');	 
												
												
											} else if($_GET['DataEntry'] == "PENDING"){ //show only paid orders that need to be picked
											print "<h1><center>Pending Orders</center></h1>";
												$stmt = $db ->prepare( 'Select *
																		FROM `Order`, OrderStatus, `Status`, (
																			SELECT Max(OrderStatus.OrderStatus_id) AS Status_id, OrderStatus.OrderStatus_Order_id AS Order_id
																			FROM OrderStatus
																			GROUP BY Order_id) i
																		WHERE `Order`.Order_id = i.Order_id
																		AND OrderStatus.OrderStatus_id = i.Status_id
																		AND OrderStatus.OrderStatus_Status_id = `Status`.Status_id
																		AND OrderStatus.OrderStatus_Status_id = 2');
													
											} else if($_GET['DataEntry'] == "PICKED"){ //show only picked orders
											print "<h1><center>Picked Orders</center></h1>";
												$stmt = $db ->prepare( 'Select *
																		FROM `Order`, OrderStatus, `Status`, (
																			SELECT Max(OrderStatus.OrderStatus_id) AS Status_id, OrderStatus.OrderStatus_Order_id AS Order_id
																			FROM OrderStatus
																			GROUP BY Order_id) i
																		WHERE `Order`.Order_id = i.Order_id
																		AND OrderStatus.OrderStatus_id = i.Status_id
																		AND OrderStatus.OrderStatus_Status_id = `Status`.Status_id
																		AND OrderStatus.OrderStatus_Status_id = 3');
													
											} else if($_GET['DataEntry'] == "SHIPPED"){ //show only shipped orders
											print "<h1><center>Shipped Orders</center></h1>";
												$stmt = $db ->prepare( 'Select *
																		FROM `Order`, OrderStatus, `Status`, (
																			SELECT Max(OrderStatus.OrderStatus_id) AS Status_id, OrderStatus.OrderStatus_Order_id AS Order_id
																			FROM OrderStatus
																			GROUP BY Order_id) i
																		WHERE `Order`.Order_id = i.Order_id
																		AND OrderStatus.OrderStatus_id = i.Status_id
																		AND OrderStatus.OrderStatus_Status_id = `Status`.Status_id
																		AND OrderStatus.OrderStatus_Status_id = 4');	
													
											} else { //by default show all orders
											print "<h1><center>All Orders</center></h1>";
											
											$stmt = $db ->prepare( 'Select *
														FROM `Order`, OrderStatus, `Status`, (
															SELECT Max(OrderStatus.OrderStatus_id) AS Status_id, OrderStatus.OrderStatus_Order_id AS Order_id
															FROM OrderStatus
															GROUP BY Order_id) i
														WHERE `Order`.Order_id = i.Order_id
														AND OrderStatus.OrderStatus_id = i.Status_id
														AND OrderStatus.OrderStatus_Status_id = `Status`.Status_id');	 
											
											}
									} 
									else  //by default show all orders
									{
									print "<h1><center>All Orders</center></h1>";
								
										$stmt = $db ->prepare( 'Select *
														FROM `Order`, OrderStatus, `Status`, (
															SELECT Max(OrderStatus.OrderStatus_id) AS Status_id, OrderStatus.OrderStatus_Order_id AS Order_id
															FROM OrderStatus
															GROUP BY Order_id) i
														WHERE `Order`.Order_id = i.Order_id
														AND OrderStatus.OrderStatus_id = i.Status_id
														AND OrderStatus.OrderStatus_Status_id = `Status`.Status_id');	 
									}
							 


if($stmt->execute())
{
	if($result = $stmt->fetchAll())
	{
echo "<table>";
echo "<tr><th>Order ID</th><th>Shopper ID</th><th>Order Date</th><th>Order Status</th><th>Last Status Change</th><th>Order Amount</th><th>View Order</th></tr>";
		foreach($result as $row)
		{
			// Format the Order_ProductAmount into en_AU currency with 4 digits of precision before 
			// the decimal point and 2 after.
			setlocale(LC_MONETARY, 'en_AU');
			$productAmountFormatted = money_format("%=0(#2.2n",$row['Order_ProductAmount']);

						echo "<tr>";
						echo "<td>".$row['Order_id']."</td>";
						echo "<td>".$row['Order_Shopper']."</td>"; 
						echo "<td>".$row['Order_TimeStamp']."</td>";
						echo "<td>".$row['Status_Name']."</td>";
						echo "<td>".$row['OrderStatus_DateTime']."</td>";
						echo "<td id='price'>"."$".$productAmountFormatted."</td>";
						echo "<td>"."<a href=/order.php?orderID=".$row['Order_id']." target='_blank'>View Order</a>"."</td>";
					echo "</tr>";
				
		
		}
		
		echo "</table>";
		
	}
}


	
?>
</div>
	</div>
	<div class="container">
		<?php include('footer.php'); ?>
	</div>
</body>
</html>
