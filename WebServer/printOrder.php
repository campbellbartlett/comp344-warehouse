<?php 
	require_once('includes/mpdf/mpdf.php');
	include "includes/connect.php";
	//Order ID is obtained from get request
	$orderID = $_GET['orderID'];
		
	//Get the shopper ID, address id from a query
	$stmt = $db -> prepare('Select *
							FROM `Order`, OrderStatus, `Status`, Shaddr, (
								SELECT Max(OrderStatus.OrderStatus_id) AS Status_id, OrderStatus.OrderStatus_Order_id AS Order_id
								FROM OrderStatus
								GROUP BY Order_id) i
							WHERE `Order`.Order_id = i.Order_id
							AND OrderStatus.OrderStatus_id = i.Status_id
							AND OrderStatus.OrderStatus_Status_id = `Status`.Status_id
							AND Order.Order_Shaddr = Shaddr.shaddr_id
							AND Order.Order_id = ?;');
	$stmt -> bindParam(1, $orderID);
	$html = "";
				
	if($stmt->execute())
	{
		if($order = $stmt->fetch())
		{
			$html .= '<html><head><title>HTML Picking Slip</title></head>
						<body>
							<table id="header" width=100%>
								<tr><td><h2>COMP344 Store</h2></td><td><h2>PACKING SLIP</h2></td></tr>
								<tr><td><p>123 Macquarie Univesity<p></td><td><p>Date/Time: '.$order["Order_TimeStamp"].'</p></td></tr>
								<tr><td><p>North Ryde, 2109</p></td><td><P>Customer ID: '.$order["Order_Shopper"].'</P></td></tr>
								<tr><td><p>Phone: 02 9499 9999</p></td><td><p></p></td></tr>
								<tr><td><p>Website: www.comp344store.com</p></td><td><p></p></td></tr>
							</table>
							<br />
							<br />
							<table>
								<tr><td><h2>SHIP TO</h2></td></tr>
								<tr><td><p>'.$order["sh_title"].' '.$order["sh_firstname"].' '.$order["sh_familyname"].'</p></td></tr>
								<tr><td><p>'.$order["sh_street1"].'</p></td></tr>
								<tr><td><p>'.$order["sh_street2"].'</p></td></tr>
								<tr><td><p>'.$order["sh_city"].'</p></td></tr>
								<tr><td><p>'.$order["sh_state"].' '.$order["sh_postcode"].'</p></td></tr>
								<tr><td><p>'.$order["sh_country"].'</p></td></tr>
							</table>';
			
			
		
			//get all the items from the order
			$stmt = $db -> prepare('SELECT * FROM OrderProduct, Product
									WHERE OP_Order_id = ?
									AND OrderProduct.OP_prod_id = Product.prod_id;');
			$stmt -> bindParam(1, $orderID);
			if($stmt->execute())
			{
				if($items = $stmt->fetchAll())
				{
					//add table header to $html
					$html .= "<br /><br />
								<h2>Order Details</h2>
								<table width=100% border=1>
									<tr><th>Product ID</th><th>Description</th><th>Qty Ordered</th><th>Weight</th></tr>";
					//check if each item has any attributes
					foreach($items as $item)
					{
						$stmt = $db -> prepare('SELECT  OrderProduct.OP_Order_id as OrderId, OrderProduct.OP_prod_id as ProductId, Product.prod_name as ProductName, Attribute.name as Attribute, AttributeValue.AttrVal_Value as AttributeValue, OrderProduct.OP_prod_id as Quantity
												FROM    OrderProduct, Attribute, AttributeValue, OrderProductAttributeValues, Product, `Order`
												WHERE   OrderProduct.OP_Order_id = OrderProductAttributeValues.OPAttr_op_id
												AND     OrderProductAttributeValues.OPAttr_Attr_id = Attribute.id
												AND     OrderProductAttributeValues.OpAttr_AttrVal_id = AttributeValue.AttrVal_id
												AND     OrderProduct.OP_prod_id = Attribute.Product_prod_id
												AND     Product.prod_id = OrderProduct.OP_prod_id
												AND		OrderProduct.OP_Order_id = ?
												AND 	OrderProduct.OP_prod_id = ?
												GROUP   BY AttributeValue.AttrVal_id');
						$stmt -> bindParam(1, $orderID);
						$stmt -> bindParam(2, $item['OP_prod_id']);
						if($stmt->execute())
						{
							//if yes print out each attribute has a new item
							if($currentItem = $stmt->fetchAll())
							{
								foreach($currentItem as $attribute)
								{
												$html .= "<tr>
															<td><p>".$attribute['ProductId']."</p></td>
															<td><p>".$attribute['ProductName']."</p><p>&nbsp;&nbsp;&nbsp;&nbsp;--".$attribute['Attribute']."; ".$attribute['AttributeValue']."</p></td>
															<td><p>".$attribute['Quantity']."</p></td>
															<td><p>".$item['prod_weight']."</p></td>
														</tr>";
											}
										}
										//otherwise just print it without any attributes
										else
										{
											$html .= "<tr>
														<td><p>".$item['OP_prod_id']."</p></td>
														<td><p>".$item['prod_name']."</p></td>
														<td><p>".$item['OP_qty']."</p></td>
														<td><p>".$item['prod_weight']."</p></td>
														</tr>";
										}
									}
								}
							}
						}
					$html .= "</table>";
					
					if($order['Order_NoteForDelivery'] != NULL)
					{
						$html .= "<br /><br /><p><strong>Note for delivery staff</strong></p><p>".$order['Order_NoteForDelivery']."</p>";
					}
					
		}
		
	}
	$html .= "</body></html>";
	$mpdf = new mPDF();
	$mpdf->WriteHTML($html);

	$mpdf->Output();

	exit;

?>