## Synopsis

Comp344 assignment - Semester 2 - 2016
Warehouse component.

## Contributors

Campbell Bartlett - Architect

Miassar Gebara - Tech writer

David Hajje - Project manger + software testing

Nicholas Jenkins - PHP + Javascript

Brenda Siddharta - Web Designer

## More information

More information is available on the comp344 assignment wiki - http://ilearn.mq.edu.au/mod/ouwiki/view.php?id=3775118&page=Warehouse%2FPacking