#!/usr/bin/env bash


apt-get update
apt-get install -y apache2 libapache2-mod-php5 libphp-phpmailer php5-mysql
service apache2 restart
if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant/WebServer /var/www
fi

